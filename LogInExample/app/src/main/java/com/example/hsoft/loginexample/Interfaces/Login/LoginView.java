package com.example.hsoft.loginexample.Interfaces.Login;

/**
 * Created by HSoft on 27/10/2017.
 */

public interface LoginView {

    void showProgress();
    void hideProgress();

    void showErrorUser();
    void showErrorPass();

    void navigatePrincipal();

}
