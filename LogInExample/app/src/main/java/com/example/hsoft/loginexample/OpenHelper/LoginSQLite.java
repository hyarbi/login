package com.example.hsoft.loginexample.OpenHelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by HSoft on 28/10/2017.
 */

public class LoginSQLite extends SQLiteOpenHelper {

    final String USER = "CREATE TABLE users(id_user INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT," +
            "name TEXT, last_name TEXT, password TEXT)";

    public LoginSQLite(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(USER);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS users");
        onCreate(db);
    }
}
