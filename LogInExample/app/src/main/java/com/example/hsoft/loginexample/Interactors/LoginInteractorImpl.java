package com.example.hsoft.loginexample.Interactors;

import android.database.sqlite.SQLiteDatabase;

import com.example.hsoft.loginexample.Interfaces.Login.LoginInteractor;
import com.example.hsoft.loginexample.Interfaces.Login.OnLoginFinishListener;
import com.example.hsoft.loginexample.OpenHelper.LoginSQLite;
import com.example.hsoft.loginexample.Views.Login;


/**
 * Created by HSoft on 27/10/2017.
 */

public class LoginInteractorImpl implements LoginInteractor {

    @Override
    public void validUser(final String user, final String pass, final OnLoginFinishListener listener,
                          Login login) {

        try {
            if(!user.equals("") && !pass.equals("")){
                LoginSQLite conexion = new LoginSQLite(login, "DB_USER", null, 1);
                SQLiteDatabase db = conexion.getReadableDatabase();

                String[] parametros = {user,pass};
                String[] campos = {"username","password"};

                listener.login();
            }else {
                if(user.equals("")){
                    listener.userError();
                }
                if(pass.equals("")){
                    listener.passError();
                }
            }
        }catch (Exception e){

        }
    }

}
