package com.example.hsoft.loginexample.Interfaces.Registro;

import com.example.hsoft.loginexample.Views.Registro;

/**
 * Created by HSoft on 28/10/2017.
 */

public interface RegistroPresenter {

    void agregarRegistro(String user, String name, String lastname, String pass,
                         Registro registro );

    void errorUsername();
    void errorName();
    void errorLastName();
    void errorPass();

    void backLogin();

}
