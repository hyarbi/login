package com.example.hsoft.loginexample.Interfaces.Login;

import com.example.hsoft.loginexample.Views.Login;

/**
 * Created by HSoft on 27/10/2017.
 */

public interface LoginPresenter {

    void validarUsuario(String user, String pass, Login login);

}
