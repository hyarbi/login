package com.example.hsoft.loginexample.Views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.hsoft.loginexample.R;

public class PrincipalMenu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.principal_menu);
    }
}
