package com.example.hsoft.loginexample.Presenters;

import com.example.hsoft.loginexample.Interactors.RegistroInteractorImpl;
import com.example.hsoft.loginexample.Interfaces.Registro.RegistroInteractor;
import com.example.hsoft.loginexample.Interfaces.Registro.RegistroPresenter;
import com.example.hsoft.loginexample.Interfaces.Registro.RegistroView;
import com.example.hsoft.loginexample.Views.Registro;

/**
 * Created by HSoft on 28/10/2017.
 */

public class RegistroPresenterImpl implements RegistroPresenter {

    private RegistroView registroView;
    private RegistroInteractor registroInteractor;

    public RegistroPresenterImpl(RegistroView registroView) {
        this.registroView = registroView;
        registroInteractor = new RegistroInteractorImpl();
    }

    @Override
    public void agregarRegistro(String user, String name, String lastname, String pass,
                                Registro registro) {
        registroInteractor.agragarUsuario(user,name,lastname,pass,this,registro);
    }

    @Override
    public void errorUsername() {
        if (registroView != null) {
            registroView.showErrorUsername();
        }
    }

    @Override
    public void errorName() {
        if (registroView != null) {
            registroView.showErrorName();
        }
    }

    @Override
    public void errorLastName() {
        if (registroView != null) {
            registroView.showErrorLastName();
        }
    }

    @Override
    public void errorPass() {
        if (registroView != null) {
            registroView.showErrorPass();
        }
    }

    @Override
    public void backLogin() {
        if (registroView != null) {
            registroView.naviateLogin();
        }
    }


}
