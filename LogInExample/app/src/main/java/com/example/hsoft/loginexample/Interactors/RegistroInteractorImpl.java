package com.example.hsoft.loginexample.Interactors;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.example.hsoft.loginexample.Interfaces.Registro.RegistroInteractor;
import com.example.hsoft.loginexample.Interfaces.Registro.RegistroPresenter;
import com.example.hsoft.loginexample.OpenHelper.LoginSQLite;
import com.example.hsoft.loginexample.Views.Registro;

/**
 * Created by HSoft on 28/10/2017.
 */

public class RegistroInteractorImpl implements RegistroInteractor {

    @Override
    public void agragarUsuario(String user, String name, String lastname, String pass,
                               RegistroPresenter registroPresenter, Registro registro) {

        LoginSQLite conexion = new LoginSQLite(registro, "DB_USER", null, 1);
        SQLiteDatabase db = conexion.getWritableDatabase();
        ContentValues values = new ContentValues();
        if (!user.equals("") && !name.equals("") && !lastname.equals("") && !pass.equals("")) {
            values.put("username",user);
            values.put("name",name);
            values.put("last_name",lastname);
            values.put("password",pass);

            Long idResultante = db.insert("users","id_user",values);
            Toast.makeText(registro.getApplicationContext(), "ID_Registro: "+idResultante,Toast
                    .LENGTH_LONG).show();
            db.close();
            registroPresenter.backLogin();
        }else{
            if (user.equals(""))
                registroPresenter.errorUsername();
            if (name.equals(""))
                registroPresenter.errorName();
            if (lastname.equals(""))
                registroPresenter.errorLastName();
            if (pass.equals(""))
                registroPresenter.errorPass();

        }

    }

}
