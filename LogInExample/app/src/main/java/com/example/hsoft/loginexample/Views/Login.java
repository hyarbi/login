package com.example.hsoft.loginexample.Views;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.hsoft.loginexample.Interfaces.Login.LoginPresenter;
import com.example.hsoft.loginexample.Interfaces.Login.LoginView;
import com.example.hsoft.loginexample.OpenHelper.LoginSQLite;
import com.example.hsoft.loginexample.Presenters.LoginPresenterImpl;
import com.example.hsoft.loginexample.R;

public class Login extends AppCompatActivity implements LoginView{

    private EditText user,pass;
    private ProgressBar progressBar;
    private LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        user = (EditText) findViewById(R.id.txtUser);
        pass = (EditText) findViewById(R.id.txtPass);
        progressBar = (ProgressBar) findViewById(R.id.prgBar);
        loginPresenter = new LoginPresenterImpl(this);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorUser() {
        user.setError("Campo Obligatorio");
    }

    @Override
    public void showErrorPass() {
        pass.setError("Campo Obligatorio");
    }

    @Override
    public void navigatePrincipal() {
        startActivity(new Intent(Login.this,PrincipalMenu.class));
    }

    public void validacion(View v){
        loginPresenter.validarUsuario(user.getText().toString(),pass.getText().toString(),this);
    }

    public void registrar(View v){
        startActivity(new Intent(Login.this,Registro.class));
    }

}
