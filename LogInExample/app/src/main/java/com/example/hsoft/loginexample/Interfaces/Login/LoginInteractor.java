package com.example.hsoft.loginexample.Interfaces.Login;

import com.example.hsoft.loginexample.Views.Login;

/**
 * Created by HSoft on 27/10/2017.
 */

public interface LoginInteractor {

    void validUser(String user, String pass, OnLoginFinishListener listener, Login login);
}
