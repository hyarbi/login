package com.example.hsoft.loginexample.Interfaces.Login;

/**
 * Created by HSoft on 28/10/2017.
 */

public interface OnLoginFinishListener {

    void userError();
    void passError();

    void login();

}
