package com.example.hsoft.loginexample.Views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.hsoft.loginexample.Interfaces.Registro.RegistroPresenter;
import com.example.hsoft.loginexample.Interfaces.Registro.RegistroView;
import com.example.hsoft.loginexample.OpenHelper.LoginSQLite;
import com.example.hsoft.loginexample.Presenters.RegistroPresenterImpl;
import com.example.hsoft.loginexample.R;

public class Registro extends AppCompatActivity implements RegistroView {

    private EditText user, name, lastname, pass;
    private RegistroPresenter registroPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registro);

        LoginSQLite conexion = new LoginSQLite(this, "DB_USER", null, 1);

        user = (EditText) findViewById(R.id.txtUser);
        name = (EditText) findViewById(R.id.txtName);
        lastname = (EditText) findViewById(R.id.txtLastName);
        pass = (EditText) findViewById(R.id.txtPass);

        registroPresenter = new RegistroPresenterImpl(this);
    }

    @Override
    public void showErrorUsername() {
        user.setError("Debe ingresar un nombre de usuario");
    }

    @Override
    public void showErrorName() {
        name.setError("Debe ingresar su nombre");
    }

    @Override
    public void showErrorLastName() {
        lastname.setError("Debe ingresar su apellido");
    }

    @Override
    public void showErrorPass() {
        pass.setError("Debe ingresar una contraseña");
    }

    @Override
    public void naviateLogin() {
        startActivity(new Intent(Registro.this,Login.class));
    }

    public void agregar(View v) {
        registroPresenter.agregarRegistro(user.getText().toString(), name.getText().toString(),
                lastname.getText().toString(), pass.getText().toString(),this);
    }


}
