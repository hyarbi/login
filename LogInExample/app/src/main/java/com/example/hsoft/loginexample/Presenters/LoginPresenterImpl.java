package com.example.hsoft.loginexample.Presenters;

import com.example.hsoft.loginexample.Interactors.LoginInteractorImpl;
import com.example.hsoft.loginexample.Interfaces.Login.LoginInteractor;
import com.example.hsoft.loginexample.Interfaces.Login.LoginPresenter;
import com.example.hsoft.loginexample.Interfaces.Login.LoginView;
import com.example.hsoft.loginexample.Interfaces.Login.OnLoginFinishListener;
import com.example.hsoft.loginexample.Views.Login;

/**
 * Created by HSoft on 27/10/2017.
 */

public class LoginPresenterImpl implements LoginPresenter, OnLoginFinishListener{

    private LoginView loginView;
    private LoginInteractor loginInteractor;

    public LoginPresenterImpl(LoginView loginView) {
        this.loginView = loginView;
        loginInteractor = new LoginInteractorImpl();
    }

    @Override
    public void validarUsuario(String user, String pass, Login login) {
        if(loginView != null){
            loginView.showProgress();
        }
        loginInteractor.validUser(user,pass,this,login);
    }

    @Override
    public void userError() {
        if(loginView != null){
            loginView.hideProgress();
            loginView.showErrorUser();
        }
    }

    @Override
    public void passError() {
        if(loginView != null){
            loginView.hideProgress();
            loginView.showErrorPass();
        }
    }

    @Override
    public void login() {
        if(loginView != null){
            loginView.hideProgress();
            loginView.navigatePrincipal();
        }
    }
}
