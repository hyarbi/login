package com.example.hsoft.loginexample.Interfaces.Registro;

/**
 * Created by HSoft on 28/10/2017.
 */

public interface RegistroView {

    void showErrorUsername();
    void showErrorName();
    void showErrorLastName();
    void showErrorPass();

    void naviateLogin();

}
