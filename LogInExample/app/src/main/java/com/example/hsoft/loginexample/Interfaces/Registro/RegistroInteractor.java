package com.example.hsoft.loginexample.Interfaces.Registro;

import com.example.hsoft.loginexample.Views.Registro;

/**
 * Created by HSoft on 28/10/2017.
 */

public interface RegistroInteractor {

    void agragarUsuario(String user, String name, String lastname, String pass,
                        RegistroPresenter registroPresenter, Registro registro);

}
